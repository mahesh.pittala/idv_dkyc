#!/usr/bin/env python
# coding: utf-8

# In[1]:


import os
import pandas as pd
import psycopg2


# In[2]:


# import json
# from pandas.io.json import json_normalize

# with open('dec1_97.json') as json_data:
#     data = json.load(json_data)
df = pd.read_csv('cust_2023-05-08_clustered_xcalls.csv')


# In[3]:

test = df.copy()


# In[4]:


test.rename(columns = {'cluster_id':'folder_name'},inplace =True)

# In[5]:


df = df.iloc[:,:-1]


# In[6]:


# df = pd.DataFrame(data.items(), columns=['folder_name', 'xcall_id'])


# In[7]:


df


# In[8]:


#df2 = df.join(df.pop('xcall_id').apply(pd.Series)).stack().reset_index()


# In[9]:


#df = df.reset_index()


# In[10]:


#df2 = df2[df2['level_1'] != 'folder_name']


# In[11]:


#df3 = df.merge(df2, left_on = 'index',right_on = 'level_0' )
#df3.columns = ['index','folder_name','level_0','level_1','xcall_id']
#df4  = df3[['folder_name','xcall_id']]


# In[12]:


df4 = df.copy()


# In[13]:


df4.columns = ['xcall_id','folder_name']


# In[14]:


df4


# In[15]:


df4.folder_name.nunique()


# In[16]:


df4.xcall_id.nunique()


# In[17]:


len(df4.xcall_id)


# In[18]:


db_params = {
"host" : "10.79.0.23",
"port" : 5432,
"dbname" :  "short_term_database",
"user" : "postges@jiovishwam-production-ephemeral-1-dupe",
"password" : "Q4fu5OK6gNOf90D8yC",
"sslmode" : "require"
}
connection = psycopg2.connect(**db_params)

a = "select orn,xcall_id,created_at from transactions where "


# In[19]:


main_df = pd.DataFrame()


# In[20]:


main_df = pd.DataFrame()
for i in range(0,len(df4['xcall_id']),10000):
     req_xcalls = df4['xcall_id'][i:i+10000].to_list()
     temp = pd.read_sql(sql=a  + " xcall_id in (" + str(list(req_xcalls))[1:-1] +")", con = connection)
     main_df = pd.concat([main_df,temp])



# In[21]:



# In[23]:


main_df.xcall_id.nunique()


# In[24]:


main_df_v2 = df4.merge(main_df, on = 'xcall_id')


# In[25]:


main_df_v2


# In[26]:


main_df_v2['created_at'] = pd.to_datetime(main_df_v2['created_at'], utc=True)


# In[27]:


main_df_v2.shape


# In[28]:


main_df_v3 = main_df_v2.assign(
    timediff=main_df_v2.sort_values(
        'created_at', ascending=False
    ).groupby(['folder_name']).created_at.diff(-1).dt.seconds.div(60).round().fillna(0))


# In[29]:


#temp_v2 = main_df_v3[main_df_v3['timediff'] > 0]


# In[30]:


main_df_v3


# In[31]:


main_df_v3['rank'] = main_df_v3.groupby(['orn'])['created_at'].rank(ascending = False,method='first')
main_df_v3 = main_df_v3.sort_values(by=["orn","rank"], ignore_index=True)


# In[32]:


main_df_v10 = main_df_v3[main_df_v3['rank'] == 1]


# In[33]:


#main_df_v10 = main_df_v10[main_df_v10['timediff'] > 0]


# In[34]:


main_df_v4  = main_df_v10.groupby('folder_name').agg(list)


# In[35]:


main_df_v10


# In[36]:


main_df_v4['orn_count'] = main_df_v4.orn.apply(len)
main_df_v4['max_time'] = main_df_v4.timediff.apply(max)


# In[37]:


main_df_v4 = main_df_v4.sort_values(by=['max_time','orn_count'],ascending = [False,False]).reset_index()


# In[38]:


# temp_v2.merge(main_df_v4[main_df_v4['orn_count'] > 1][['orn_count','folder_name']], on = 'folder_name', how = 'inner')[['xcall_id','folder_name']].to_csv('orn_xcalls_dec20_97_pic_download.csv',index = 0)


# In[39]:


main_df_v4


# In[40]:


main_df_v4.to_csv('output/orn_xcalls_May8_94.csv',index = 0)


# # phase1

# In[41]:


main_df_v4_temp = main_df_v4[(main_df_v4['max_time'] > 0) & (main_df_v4['orn_count'] > 1) ]


# In[42]:



main_df_v11 = main_df_v10.merge(main_df_v4_temp[['folder_name']] , on = 'folder_name')


# In[43]:


main_df_v11.xcall_id.nunique()


# In[44]:


main_df1_v4  = main_df_v11.groupby('folder_name').agg(list)


# In[45]:


main_df1_v4['orn_count'] = main_df1_v4.orn.apply(len)
main_df1_v4['max_time'] = main_df1_v4.timediff.apply(max)


# In[46]:


main_df1_v4 = main_df1_v4.sort_values(by=['max_time','orn_count'],ascending = [False,False]).reset_index()


# In[48]:


print_df = main_df_v3.merge(main_df1_v4[main_df1_v4['orn_count'] > 1][['orn_count','folder_name']], on = 'folder_name', how = 'inner')[['xcall_id','folder_name']].drop_duplicates()
print(print_df.shape)

# In[49]:
result_df = print_df.merge(test,how = 'left')
print(result_df.shape)

final_result_df = result_df.groupby(['folder_name','distance'],as_index = False).first().sort_values(['folder_name','distance'],ascending = False)

final_result_df[['xcall_id','folder_name']].to_csv('output/orn_xcalls_May8_94_pic_download.csv',index = 0)





# %%
