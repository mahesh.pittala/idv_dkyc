#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import glob
import psycopg2
from datetime import datetime, timedelta
import psycopg2.extras as extras


# In[2]:



import warnings
warnings.filterwarnings('ignore')


# In[3]:


from datetime import date

date.today()


# In[4]:



# In[5]:


# In[ ]:





# In[76]:


fp_df = pd.read_csv('agent_orn.csv')


# In[6]:


fp_df.columns = ['ORN']


# In[7]:


fp_df


# In[79]:


fp_df.shape


# In[80]:


query='''select xcall_id,a.orn,properties->>'is_back' as is_back,req_url,properties->> 'capture_type'  as capture_type,properties->'result'->>'match-score'  as SufiScore,properties->>'storeId'  as store_id,properties->> 'match_type' as match_type,response_code,properties->>'is_live' as is_live,properties->>'journey' as journey,properties->>'starttime' as starttime
from transactions a
where  req_url  in ('/v1/read_document','/v1/check_liveness','/v1/face_match')   '''


# In[81]:


query_de = '''select distinct orn,properties->>'is_back' as is_back  from transactions where  req_url = '/v1/read_document' and properties->>'journey' = 'na'  '''


# In[82]:


query_qr = '''select distinct orn  from transactions where  req_url = '/v1/read_document' and properties->>'journey' <> 'na' and properties->>'journey' <> 'ekyc' and properties->>'journey' <> 'ea'  '''


# In[83]:


db_params = {
"host" : "10.79.0.26",
"port" : 5432,
"dbname" :  "short_term_database",
"user" : "postges@jiovishwam-production-ephemeral-1-dupe",
"password" : "Q4fu5OK6gNOf90D8yC",
"sslmode" : "require"
}

connection = psycopg2.connect(**db_params)


# In[84]:




# In[85]:



# In[90]:




# In[91]:


data1 = pd.read_sql(sql=query + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)


# In[92]:


data1


# In[93]:


data1['starttime'] = pd.to_datetime(data1['starttime'], utc=True)
data1['starttime'] = data1['starttime'].dt.tz_convert('Asia/Kolkata')
data1.sufiscore.fillna('-1',inplace=True)
data1['sufiscore'] = data1['sufiscore'].astype(str).astype(int)


# In[94]:


data1


# In[95]:


data1.orn.nunique()


# In[96]:



def journey(data):
    data['journey_status'] = np.select([
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='0'),
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='1'),

    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['sufiscore'] < 70),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['sufiscore'] >= 70),

    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='no'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='no')
    ], 
    [
    'till_customer_liveness',
    'till_agent_liveness',
    'till_agent_agent_facematch',
    'Success',
    'till_poi',
    'till_poa',
    'till_poi',
    'till_poa',
    'till_customer_facematch',
    'till_customer_facematch',
    'till_agent_customer_facematch',
    'till_agent_customer_facematch',
    'till_customer_liveness',
    'till_agent_liveness',
    'till_customer_liveness',
    'till_agent_liveness'

    ], 
    default='Unknown'
    )

    data['journey_completion_status'] = np.select([
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='0'),
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='1'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['sufiscore'] < 30),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['sufiscore'] >= 30),


    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),

    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0') & (data['sufiscore'] < 30),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0') & (data['sufiscore'] >= 30),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),

    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1') & (data['sufiscore'] < 70),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1') & (data['sufiscore'] >= 70),


    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='no'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='no')
    ], 
    [
    'Failed',
    'Failed',
    'Failed',
    'Success',
    'Customer left',
    'Customer left',
    'Failed',
    'Failed',
    'Failed',

    'Failed',
    'Customer left',

    'Failed',

    'Passed',
    'Failed',

    'Failed',
    'Customer left',
    'Customer left',
    'Failed'



    ], 
    default='Unknown'
    )
    return data


# In[ ]:

# # QR

# In[97]:


qr= pd.read_sql(sql=query_qr + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)
   


# In[98]:



#### getting just the QR transactions ####

data_qr = data1.merge(qr, on=['orn'],how ='inner')


data_qr['starttime'] = pd.to_datetime(data_qr['starttime'], utc=True)

print("Counts of distinct orns : ", data_qr.orn.nunique())


#### Taking the last transaction ####
data_qr['rank'] = data_qr.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_qr = data_qr.sort_values(by=["orn","rank"], ignore_index=True)
data_qr_v2 = data_qr[data_qr['rank'] == 1]


#### Taking the journey status ####

data_qr_final = journey(data_qr_v2)

print( data_qr_final.shape)
print(data_qr_final.journey_status.value_counts())
print(data_qr_final.groupby(['journey_status','journey_completion_status']).orn.nunique())

print("Counts of unknown journeys : " , data_qr_final[data_qr_final['journey_status'] == 'Unknown'].shape)


data_qr_final['journey_type'] = 'qr'
data_qr_final['card_type'] = 'qr_aadhaar'
data_qr_final['match_percentage'] = data_qr_final['sufiscore']

data_qr_final2 = data_qr_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_qr_final2.head()

print( "final orn print count",data_qr_final2.shape)


# # DE

# In[99]:


data_de= pd.read_sql(sql=query_de + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)
   


# ## DE -  Voter ##

# In[100]:


#### Filtering the Voter orns ####

voter = data_de[((data_de['is_back'] == '40') | (data_de['is_back'] == '41'))]['orn'].drop_duplicates()
#print(len(data_voter))


#### getting just the voter transactions ####

data_voter = data1.merge(voter, on=['orn'],how ='inner')


data_voter['starttime'] = pd.to_datetime(data_voter['starttime'], utc=True)
print("Counts of distinct voter orns : ", data_voter.orn.nunique())

#### Taking the last transaction ####
data_voter['rank'] = data_voter.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_voter = data_voter.sort_values(by=["orn","rank"], ignore_index=True)
data_voter_v2 = data_voter[data_voter['rank'] == 1]


#### Taking the journey status ####
data_voter_final = journey(data_voter_v2)


print( data_voter_final.shape)
print(data_voter_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_voter_final[data_voter_final['journey_status'] == 'Unknown'].shape)


data_voter_final['journey_type'] = 'de'
data_voter_final['card_type'] = 'voter'
data_voter_final['match_percentage'] = data_voter_final['sufiscore']


data_voter_final2 = data_voter_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_voter_final2.head()

print( "final orn print count",data_voter_final2.shape)


# In[101]:


print(data_voter_final.groupby(['journey_status','journey_completion_status']).orn.nunique())


# ## DE Aadhaar ##

# In[102]:


#### Filtering the Aadhaar orns ####

aadhaar = data_de[(data_de['is_back'] == '00') | (data_de['is_back'] == '01')]['orn'].drop_duplicates()


#### getting just the voter transactions ####

data_aadhaar = data1.merge(aadhaar, on=['orn'],how ='inner')
print(len(data_aadhaar))


data_aadhaar['starttime'] = pd.to_datetime(data_aadhaar['starttime'], utc=True)
print("Counts of distinct aadhaar orns : ", data_aadhaar.orn.nunique())



#### Taking the last transaction ####
data_aadhaar['rank'] = data_aadhaar.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_aadhaar = data_aadhaar.sort_values(by=["orn","rank"], ignore_index=True)
data_aadhaar_v2 = data_aadhaar[data_aadhaar['rank'] == 1]


#### Taking the journey status ####

data_aadhaar_final = journey(data_aadhaar_v2)



print(data_aadhaar_final.shape)
print(data_aadhaar_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_aadhaar_final[data_aadhaar_final['journey_status'] == 'Unknown'].shape)


data_aadhaar_final['journey_type'] = 'de'
data_aadhaar_final['card_type'] = 'aadhaar'
data_aadhaar_final['match_percentage'] = data_aadhaar_final['sufiscore']


data_aadhaar_final2 = data_aadhaar_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_aadhaar_final2.head()

print( "final orn print count",data_aadhaar_final2.shape)


# ## DE - PAN

# In[103]:


#### Filtering the PAN orns ####

pan = data_de[(data_de['is_back'] == '20')]['orn'].drop_duplicates()

#### getting just the voter transactions ####

data_pan = data1.merge(pan, on=['orn'],how ='inner')
print(len(data_pan))


data_pan['starttime'] = pd.to_datetime(data_pan['starttime'], utc=True)
print("Counts of distinct pan orns : ", data_pan.orn.nunique())

#### Taking the last transaction ####
data_pan['rank'] = data_pan.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_pan = data_pan.sort_values(by=["orn","rank"], ignore_index=True)
data_pan_v2 = data_pan[data_pan['rank'] == 1]


#### Taking the journey status ####

data_pan_final = journey(data_pan_v2)


print(data_pan_final.shape)
print(data_pan_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_pan_final[data_pan_final['journey_status'] == 'Unknown'].shape)


data_pan_final['journey_type'] = 'de'
data_pan_final['card_type'] = 'pan'
data_pan_final['match_percentage'] = data_pan_final['sufiscore']


data_pan_final2 = data_pan_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_pan_final2.head()

print( "final orn print count",data_pan_final2.shape)


# ## DE - Passport

# In[104]:


#### Filtering the Passport orns ####

passport = data_de[((data_de['is_back'] == '30') | (data_de['is_back'] == '31'))]['orn'].drop_duplicates()
#### getting just the voter transactions ####

data_passport = data1.merge(passport, on=['orn'],how ='inner')
print("Counts of distinct passport orns : ", data_passport.orn.nunique())



data_passport['starttime'] = pd.to_datetime(data_passport['starttime'], utc=True)
print(data_passport.orn.nunique())

#### Taking the last transaction ####
data_passport['rank'] = data_passport.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_passport = data_passport.sort_values(by=["orn","rank"], ignore_index=True)
data_passport_v2 = data_passport[data_passport['rank'] == 1]


#### Taking the journey status ####

data_passport_final = journey(data_passport_v2)



print(data_passport_final.shape)
print(data_passport_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_passport_final[data_passport_final['journey_status'] == 'Unknown'].shape)


data_passport_final['journey_type'] = 'de'
data_passport_final['card_type'] = 'passport'
data_passport_final['match_percentage'] = data_passport_final['sufiscore']


data_passport_final2 = data_passport_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_passport_final2.head()

print( "final orn print count",data_passport_final2.shape)


# ## EA ##

# In[105]:


query_ea = '''select distinct orn  from transactions where  req_url = '/v1/read_document'  and properties->>'journey' = 'ea'  '''


# In[106]:


ea= pd.read_sql(sql=query_ea + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)
   


# In[107]:



#### getting just the ea transactions ####

data_ea = data1.merge(ea, on=['orn'],how ='inner')


data_ea['starttime'] = pd.to_datetime(data_ea['starttime'], utc=True)
print("Counts of distinct ea aadhaar orns : ", data_ea.orn.nunique())

#### Taking the last transaction ####
data_ea['rank'] = data_ea.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_ea = data_ea.sort_values(by=["orn","rank"], ignore_index=True)
data_ea_v2 = data_ea[data_ea['rank'] == 1]


#### Taking the journey status ####

data_ea_final = journey(data_ea_v2)


print( data_ea_final.shape)
print(data_ea_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_ea_final[data_ea_final['journey_status'] == 'Unknown'].shape)


data_ea_final['journey_type'] = 'de'
data_ea_final['card_type'] = 'e_aadhaar'
data_ea_final['match_percentage'] = data_ea_final['sufiscore']


data_ea_final2 = data_ea_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_ea_final2.head()

print( "final orn print count",data_ea_final2.shape)


# In[108]:


data_ea_final2


# In[109]:



### QR ###
print( "final orn print count of QR",data_qr_final2.shape)

###  DE ###
print( "final orn print count of Aadhaar",data_aadhaar_final2.shape)
print( "final orn print count of voter",data_voter_final2.shape)
print( "final orn print count of PAN",data_pan_final2.shape)
print( "final orn print count of Passport",data_passport_final2.shape)


### EA ###
print( "final orn print count of EA",data_ea_final2.shape)


####### Jurney count #####
print("final journey count of QR",data_qr_final2.journey_status.value_counts())
print("final journey count of Aadhaar",data_aadhaar_final2.journey_status.value_counts())
print("final journey count of voter",data_voter_final2.journey_status.value_counts())
print("final journey count of PAN",data_pan_final2.journey_status.value_counts())
print("final journey count of Passport",data_passport_final2.journey_status.value_counts())
print("final journey count of EA",data_ea_final2.journey_status.value_counts())



# In[110]:


data_qr_final2['date']  = data_qr_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date
data_aadhaar_final2['date']  = data_aadhaar_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date
data_voter_final2['date']  = data_voter_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date
data_pan_final2['date']  = data_pan_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date
data_passport_final2['date']  = data_passport_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date
data_ea_final2['date']  = data_ea_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date


# ## E KYC ##

# In[111]:


query_ekyc = '''select distinct orn  from transactions where  req_url = '/v1/check_liveness' and properties->>'journey' = 'ekyc'  '''


# In[112]:


data1.orn.nunique()


# In[113]:


data1[data1['journey'] == 'ekyc']


# In[114]:


ekyc= pd.read_sql(sql=query_ekyc + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)
 


# In[115]:


query_ekyc + "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")"


# In[116]:


ekyc


# In[117]:



#### getting just the ekyc transactions ####

data_ekyc = data1.merge(ekyc, on=['orn'],how ='inner')


data_ekyc['starttime'] = pd.to_datetime(data_ekyc['starttime'], utc=True)
print("Counts of distinct ekyc aadhaar orns : ", data_ekyc.orn.nunique())

#### Taking the last transaction ####
data_ekyc['rank'] = data_ekyc.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_ekyc = data_ekyc.sort_values(by=["orn","rank"], ignore_index=True)
data_ekyc_final = data_ekyc[data_ekyc['rank'] == 1]
data_ekyc_final.sufiscore.fillna('-1',inplace=True)
data_ekyc_final['sufiscore'] = data_ekyc_final['sufiscore'].astype(str).astype(int)


# In[118]:


data_ekyc_new = data_ekyc.copy()
data_ekyc_new['match_type'] = data_ekyc_new['match_type'].replace('',100)

data_ekyc_new['match_type'] = data_ekyc_new['match_type'].astype(str).astype(int)

data_ekyc_new.match_type.fillna(100,inplace=True)

data_ekyc_temp = data_ekyc_new[data_ekyc_new['orn'].isin(data_ekyc_new[data_ekyc_new['req_url'] ==  '/v1/face_match']['orn'])].groupby('orn').agg({'match_type' : np.min}).reset_index()


# In[119]:


data_ekyc_temp['min_match'] = data_ekyc_temp['match_type']
data_ekyc_temp = data_ekyc_temp[['orn','min_match']]
data_ekyc_final = data_ekyc_final.merge(data_ekyc_temp, on = 'orn', how = 'left')


# In[120]:




#### Taking the journey status ####
data_ekyc_final['journey_status'] = np.select(
    [
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417))) & (data_ekyc_final['capture_type'] == '0'), 
                
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['sufiscore'] >= 30),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['sufiscore'] < 30),
        
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] >= 70),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] < 70) & (data_ekyc_final['min_match'] == 2),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] < 70) & (data_ekyc_final['min_match'] != 2),
        
        
        ((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417)) &   (data_ekyc_final['req_url'] == '/v1/face_match') &  (data_ekyc_final['match_type'] == '0'),
        
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'no'), 
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'yes'), 
        

   
    ], 
    [
        'till_customer_liveness', 
        'Success',
        'till_customer_customer_facematch',
        
        'till_agent_customer_facematch',
        'till_agent_customer_facematch',
        'Unknown-till_agent_customer_facematch',
        
        'till_customer_facematch',
        'till_customer_liveness',
        'till_customer_liveness',
       
    ], 
    default='Unknown'
)

data_ekyc_final['journey_completion_status'] = np.select(
    [
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417))) & (data_ekyc_final['capture_type'] == '0'), 
                
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['sufiscore'] >= 30),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['sufiscore'] < 30),
        
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] >= 70),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] < 70) & (data_ekyc_final['min_match'] == 2),
        (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['sufiscore'] < 70) & (data_ekyc_final['min_match'] != 2),
        
        
        ((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417)) &   (data_ekyc_final['req_url'] == '/v1/face_match') &  (data_ekyc_final['match_type'] == '0'),
        
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'no'), 
        (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'yes'), 
        

   
    ], 
    [
        'Failed', 
        'Success',
        'Failed',
        
        'Failed',
        'Customer left',
        'Customer left',
        
        'Failed',
        'Failed',
        'Customer left',
       
    ], 
    default='Unknown'
)


# In[121]:



print( data_ekyc_final.shape)
print(data_ekyc_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data_ekyc_final[data_ekyc_final['journey_status'] == 'Unknown'].shape)


data_ekyc_final['journey_type'] = 'ekyc'
data_ekyc_final['card_type'] = 'ekyc'
data_ekyc_final['match_percentage'] = data_ekyc_final['sufiscore']


data_ekyc_final2 = data_ekyc_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_ekyc_final2.head()

print( "final orn print count",data_ekyc_final2.shape)

print("final journey count of Ekyc",data_ekyc_final2.journey_status.value_counts())

data_ekyc_final2['date']  = data_ekyc_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date


# In[122]:


final_journey = pd.concat([data_aadhaar_final2[['orn','card_type','journey_status']],data_voter_final2[['orn','card_type','journey_status']],data_pan_final2[['orn','card_type','journey_status']],data_passport_final2[['orn','card_type','journey_status']],data_ea_final2[['orn','card_type','journey_status']],data_ekyc_final2[['orn','card_type','journey_status']],data_qr_final2[['orn','card_type','journey_status']]]).reset_index()


# In[123]:


final_journey


# In[126]:


#data1.merge(final_journey,on ='orn')
data1_noread = data1[~data1.orn.isin(final_journey.orn)]


# In[127]:


data1_noread.orn.nunique()


# In[128]:


#### Filtering the Passport orns ####

data1_noread['starttime'] = pd.to_datetime(data1_noread['starttime'], utc=True)
print(data1_noread.orn.nunique())

#### Taking the last transaction ####
data1_noread['rank'] = data1_noread.groupby("orn")["starttime"].rank(ascending=False,method='first')
data1_noread = data1_noread.sort_values(by=["orn","rank"], ignore_index=True)
data1_noread_v2 = data1_noread[data1_noread['rank'] == 1]


#### Taking the journey status ####

data1_noread_final = journey(data1_noread_v2)



print(data1_noread_final.shape)
print(data1_noread_final.journey_status.value_counts())
print("Counts of unknown journeys : " , data1_noread_final[data1_noread_final['journey_status'] == 'Unknown'].shape)


data1_noread_final['journey_type'] = 'de'
data1_noread_final['card_type'] = 'noread_doc'
data1_noread_final['match_percentage'] = data1_noread_final['sufiscore']


data1_noread_final2 = data1_noread_final[['orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data1_noread_final2.head()

print( "final orn print count",data1_noread_final2.shape)


# In[129]:


data1_noread_final2


# In[130]:


data1_noread_final2[['orn','card_type','journey_status']]


# In[131]:


data1[~data1.orn.isin(final_journey.orn)].orn.unique()


# In[132]:


final_journey = pd.concat([final_journey[['orn','card_type','journey_status']],data1_noread_final2[['orn','card_type','journey_status']]])


# In[134]:


final_journey.to_csv('check_journey_agent.csv',index = 0)


# In[ ]:




