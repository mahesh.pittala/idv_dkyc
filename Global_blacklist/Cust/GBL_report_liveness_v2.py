#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import glob
import psycopg2
from datetime import datetime, timedelta
import psycopg2.extras as extras
from datetime import date


# In[2]:


import warnings
warnings.filterwarnings('ignore')


# In[3]:


db_params = {
"host" : "10.79.0.23",
"port" : 5432,
"dbname" :  "short_term_database",
"user" : "postges@jiovishwam-production-ephemeral-1-dupe",
"password" : "Q4fu5OK6gNOf90D8yC",
"sslmode" : "require"
}

connection = psycopg2.connect(**db_params)


# In[4]:


fp_df = pd.read_csv('cust_orn.csv')
fp_df.columns = ['ORN']


# In[6]:


fp_df


# In[10]:


query='''select orn,store_id,xcall_id as prod_xcall_id,properties->>'journey' as journey, properties->>'capture_type' as capture_type,properties->>'gbl_match_id' as gbl_xcall_id,properties->>'gbl_max_userid' as gbl_user_id , properties->>'gbl_max_dist' as max_dist , properties->>'gbl_match_indb' as indb , properties->>'dc' as isdc,properties->>'dc_response' as dc_response,properties->>'dc_accept' as dc_accept,properties->>'dc_reject' as dc_reject, properties->>'dc_resp' as dc_rejection, properties->'similarBlFaces' as gbl_list, created_at ,properties->>'starttime' as starttime from transactions where req_url='/v1/check_liveness'   '''


# In[11]:


def execute_values(conn, df, table):
    tuples = [tuple(x) for x in df.to_numpy()]
    cols = ','.join(list(df.columns))
    query  = "INSERT INTO %s(%s) VALUES %%s" % (table, cols)
    cursor = conn.cursor()
    try:
        extras.execute_values(cursor, query, tuples)
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("execute_values() done")
    cursor.close()


# In[12]:


dump = pd.read_sql(sql=query+ "and orn in (" + str(list(fp_df['ORN']))[1:-1] +")", con = connection,coerce_float = False)


# In[13]:


dump.head()


# In[15]:


dump.orn.nunique()


# ### Get the journey status
# 
# 

# In[49]:


query='''select xcall_id,a.orn,properties->>'is_back' as is_back,req_url,properties->>'gbl_match_id' as gbl_xcall_id,properties->> 'capture_type'  as capture_type,properties->'result'->>'match-score'  as MatchScore,properties->>'storeId'  as store_id,properties->> 'match_type' as match_type,response_code,properties->>'is_live' as is_live,properties->>'journey' as journey,properties->>'starttime' as starttime,created_at,
properties->>'poaOcrConf' as poaOcrConf,properties->>'poiOcrConf' as  poiOcrConf,properties->>'gbl_match_id' as gbl_xcall_id,properties->>'gbl_max_userid' as gbl_user_id , properties->>'gbl_max_dist' as max_dist , properties->>'gbl_match_indb' as indb , properties->>'dc' as isdc from transactions a
where  req_url  in ('/v1/read_document','/v1/check_liveness','/v1/face_match')  '''


# In[50]:


data1 = pd.read_sql(sql=query+  "and orn in (" + str(list(dump['orn']))[1:-1] +")", con = connection,coerce_float = False)


# In[51]:


data1


# In[52]:


data1.orn.nunique()


# In[53]:


data1['starttime'] = pd.to_datetime(data1['starttime'], utc=True)
data1['starttime'] = data1['starttime'].dt.tz_convert('Asia/Kolkata')
data1.matchscore.fillna('-1',inplace=True)
data1['matchscore'] = data1['matchscore'].astype(str).astype(int)
data1 = data1.rename(columns={"matchscore": "MatchScore"})


# In[54]:


df=data1[data1['req_url'] == '/v1/read_document'][['orn','journey','is_back']].drop_duplicates()



df['card_type'] = 'other'
df.loc[((df['journey'] != 'na') & (df['journey'] != 'ekyc') & (df['journey'] != 'ea')), 'card_type'] = 'qr_aadhaar'
df.loc[((df['journey'] == 'na') & ((df['is_back'] == '40') | (df['is_back'] == '41'))), 'card_type'] = 'voter'
df.loc[((df['journey'] == 'na') & ((df['is_back'] == '00') | (df['is_back'] == '01')| (df['is_back'] == '1')| (df['is_back'] == '0'))), 'card_type'] = 'aadhaar'
df.loc[((df['journey'] == 'na') & ((df['is_back'] == '30') | (df['is_back'] == '31'))), 'card_type'] = 'passport'
df.loc[((df['journey'] == 'na') & ((df['is_back'] == '20') )), 'card_type'] = 'pan'
df.loc[((df['journey'] == 'ea')), 'card_type'] = 'e_aadhaar'

df['journey_type'] = 'other'
df.loc[((df['journey'] == 'ekyc') ), 'journey_type'] = 'ekyc'
df.loc[((df['journey'] != 'na') & (df['journey'] != 'ekyc') & (df['journey'] != 'ea')), 'journey_type'] = 'qr'
df.loc[((df['journey'] == 'na') | (df['journey'] == 'ea')), 'journey_type'] = 'de'

df_dkyc = df[['orn','journey_type','card_type']].drop_duplicates()


# In[55]:


df_ekyc=data1[(data1['req_url'] == '/v1/check_liveness') & (data1['journey'] == 'ekyc')][['orn']].drop_duplicates()
df_ekyc['card_type'] = 'ekyc'
df_ekyc['journey_type'] = 'ekyc'


# In[56]:


df_orn = (pd.concat([df_dkyc,df_ekyc], ignore_index=True, sort =False).drop_duplicates(['orn'], keep='last'))


# In[57]:


data1_noread = data1[~data1.orn.isin(df_orn.orn)][['orn']].drop_duplicates()


# In[58]:


data1_noread['card_type'] = 'noread_doc'
data1_noread['journey_type'] = 'de'


# In[59]:


df_orn_v2 = (pd.concat([df_orn,data1_noread[['orn','card_type','journey_type']]], ignore_index=True, sort =False).drop_duplicates(['orn'], keep='last'))
data1 = data1.merge(df_orn_v2,on = 'orn')


# In[60]:


data1


# In[61]:


data1.orn.nunique()


# In[62]:


def journey_ekyc(data_ekyc_final):
    #### Taking the journey status ####
    data_ekyc_final['journey_status'] = np.select(
        [
            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417))) & (data_ekyc_final['capture_type'] == '0'), 

            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['MatchScore'] >= 30),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['MatchScore'] < 30),

            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] >= 70),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] < 70) & (data_ekyc_final['min_match'] == 2),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] < 70) & (data_ekyc_final['min_match'] != 2),


            ((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417)) &   (data_ekyc_final['req_url'] == '/v1/face_match') &  (data_ekyc_final['match_type'] == '0'),

            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'no'), 
            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'yes'), 



        ], 
        [
            'till_customer_liveness', 
            'Success',
            'till_customer_customer_facematch',

            'till_agent_customer_facematch',
            'till_agent_customer_facematch',
            'Unknown-till_agent_customer_facematch',

            'till_customer_facematch',
            'till_customer_liveness',
            'till_customer_liveness',

        ], 
        default='Unknown'
    )

    data_ekyc_final['journey_completion_status'] = np.select(
        [
            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417))) & (data_ekyc_final['capture_type'] == '0'), 

            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['MatchScore'] >= 30),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '0') & (data_ekyc_final['MatchScore'] < 30),

            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] >= 70),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] < 70) & (data_ekyc_final['min_match'] == 2),
            (data_ekyc_final['response_code'] == 200) &  (data_ekyc_final['req_url'] == '/v1/face_match') & (data_ekyc_final['match_type'] == '2') & (data_ekyc_final['MatchScore'] < 70) & (data_ekyc_final['min_match'] != 2),


            ((data_ekyc_final['response_code'] == 422) | (data_ekyc_final['response_code'] == 417)) &   (data_ekyc_final['req_url'] == '/v1/face_match') &  (data_ekyc_final['match_type'] == '0'),

            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'no'), 
            (data_ekyc_final['req_url'] == '/v1/check_liveness') &  (data_ekyc_final['response_code'] == 200) & (data_ekyc_final['capture_type'] == '0') & (data_ekyc_final['is_live'] == 'yes'), 



        ], 
        [
            'Failed', 
            'Success',
            'Failed',

            'Failed',
            'Customer left',
            'Customer left',

            'Failed',
            'Failed',
            'Customer left',

        ], 
        default='Unknown'
    )
    return data_ekyc_final


# In[63]:


def journey(data):

    #### Taking the journey status ####
    data['journey_status'] = np.select([
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='0'),
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='1'),
        
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['MatchScore'] < 70),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['MatchScore'] >= 70),

    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='no'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='no')
    ], 
    [
    'till_customer_liveness',
    'till_agent_liveness',
    'till_agent_agent_facematch',
    'Success',
    'till_poi',
    'till_poa',
    'till_poi',
    'till_poa',
    'till_customer_facematch',
    'till_customer_facematch',
    'till_agent_customer_facematch',
    'till_agent_customer_facematch',
    'till_customer_liveness',
    'till_agent_liveness',
    'till_customer_liveness',
    'till_agent_liveness'

    ], 
    default='Unknown'
    )

    data['journey_completion_status'] = np.select([
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='0'),
    (data['req_url']=='/v1/check_liveness') & ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['capture_type']=='1'),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['MatchScore'] < 30),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='2') & (data['MatchScore'] >= 30),
        
    
    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    (data['response_code']==200) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '00') | (data['is_back'] == '0') | (data['is_back'] == '30') | (data['is_back'] == '20')| (data['is_back'] == '40') | (data['is_back'] == '11') | (data['is_back'] == '13')),
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/read_document') & ((data['is_back'] == '41') | (data['is_back'] == '01') | (data['is_back'] == '31') | (data['is_back'] == '1') | (data['is_back'] == '12') | (data['is_back'] == '14')),
    
    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0'),
    
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0') & (data['MatchScore'] < 30),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='0') & (data['MatchScore'] >= 30),

    ((data['response_code'] == 422) | (data['response_code'] == 417)) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1'),

    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1') & (data['MatchScore'] < 70),
    (data['response_code']==200) & (data['req_url']=='/v1/face_match') & (data['match_type']=='1') & (data['MatchScore'] >= 70),


    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='no'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='0') & (data['is_live']=='yes'),
    (data['req_url']=='/v1/check_liveness') & (data['response_code']==200) & (data['capture_type']=='1') & (data['is_live']=='no')
    ], 
    [
    'Failed',
    'Failed',
    'Failed',
    'Success',
    'Customer left',
    'Customer left',
    'Failed',
    'Failed',
    'Failed',

    'Failed',
    'Customer left',

    'Failed',

    'Passed',
    'Failed',

    'Failed',
    'Customer left',
    'Customer left',
    'Failed'



    ], 
    default='Unknown'
    )

    return data


# ### DKYC Journey

# In[64]:


data1


# In[65]:


data_dkyc = data1[data1['journey_type'] != 'ekyc']
data_dkyc['starttime'] = pd.to_datetime(data_dkyc['starttime'], utc=True)

print("Counts of distinct orns : ", data_dkyc.orn.nunique())


#### Taking the last transaction ####
data_dkyc['rank'] = data_dkyc.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_dkyc = data_dkyc.sort_values(by=["orn","rank"], ignore_index=True)
data_dkyc_v2 = data_dkyc[data_dkyc['rank'] == 1]


#### Taking the journey status ####
data_dkyc_final = journey(data_dkyc_v2)


data_dkyc_final['match_percentage'] = data_dkyc_final['MatchScore']
data_dkyc_final['date']  = data_dkyc_final['starttime'].dt.tz_convert('Asia/Kolkata').dt.date

data_dkyc_final2 = data_dkyc_final[['date','orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]
data_dkyc_final2.head()

print( "final orn print count",data_dkyc_final2.shape)


# ### EKYC journey

# In[66]:


#### getting just the ekyc transactions ####

data_ekyc = data1[data1['journey_type'] == 'ekyc']

data_ekyc['match_type'] = data_ekyc['match_type'].replace('',100)
data_ekyc['match_type'] = data_ekyc['match_type'].astype(str).astype(int)


data_ekyc['match_type_new'] = data_ekyc['match_type']
#df.loc[((df['journey'] != 'ekyc') & (df['journey'] != 'ea')), 'card_type'] = 'qr_aadhaar'
data_ekyc.loc[(((data_ekyc['match_type'] != 0))), 'match_type_new'] = 2

data_ekyc['match_type'] = data_ekyc['match_type_new']


data_ekyc['starttime'] = pd.to_datetime(data_ekyc['starttime'], utc=True)

#### Taking the last transaction ####
data_ekyc['rank'] = data_ekyc.groupby("orn")["starttime"].rank(ascending=False,method='first')
data_ekyc = data_ekyc.sort_values(by=["orn","rank"], ignore_index=True)
data_ekyc_final = data_ekyc[data_ekyc['rank'] == 1]
data_ekyc_final.MatchScore.fillna('-1',inplace=True)
data_ekyc_final['MatchScore'] = data_ekyc_final['MatchScore'].astype(str).astype(int)

data_ekyc_new = data_ekyc.copy()
data_ekyc_new['match_type'] = data_ekyc_new['match_type'].replace('',100)

data_ekyc_new['match_type'] = data_ekyc_new['match_type'].astype(str).astype(int)

data_ekyc_new.match_type.fillna(100,inplace=True)

data_ekyc_temp = data_ekyc_new[data_ekyc_new['orn'].isin(data_ekyc_new[data_ekyc_new['req_url'] ==  '/v1/face_match']['orn'])].groupby('orn').agg({'match_type' : np.min}).reset_index()

data_ekyc_temp['min_match'] = data_ekyc_temp['match_type']
data_ekyc_temp = data_ekyc_temp[['orn','min_match']]
data_ekyc_final = data_ekyc_final.merge(data_ekyc_temp, on = 'orn', how = 'left')

data_ekyc_final['match_type'] = data_ekyc_final['match_type'].astype(str)

data_ekyc_final2 = journey_ekyc(data_ekyc_final)

data_ekyc_final2['match_percentage'] = data_ekyc_final2['MatchScore']
data_ekyc_final2['date']  = data_ekyc_final2['starttime'].dt.tz_convert('Asia/Kolkata').dt.date


data_ekyc_final3 = data_ekyc_final2[['date','orn','journey_status','journey_completion_status','match_percentage','journey_type','card_type','xcall_id','store_id','is_back','req_url','capture_type','match_type','response_code','is_live','journey','starttime']]



# In[67]:


final_journey = pd.concat([data_ekyc_final3,data_dkyc_final2])


# In[68]:


final_journey.groupby(['journey_status']).agg({'orn' :'count'})


# In[69]:


final_journey.shape


# In[70]:


final_journey


# In[71]:


final_journey.orn.nunique()


# In[72]:


data1  =data1.merge(final_journey[['orn','journey_status']],on = 'orn')


# In[73]:


data1


# In[83]:


data2 = data1[(data1['journey_status'] == 'Success') & (data1['req_url'] == '/v1/check_liveness') ]


# In[84]:


data2.shape


# In[85]:


data2


# In[88]:


data2


# In[90]:


data2['rank'] = data2.groupby(["orn","capture_type"])["starttime"].rank(ascending=False,method='first')
data2 = data2.sort_values(by=["orn","capture_type","rank"], ignore_index=True)
data3 = data2[data2['rank'] == 1]


# In[92]:


data3.to_csv('final_journey_scores.csv',index = 0)


# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:




