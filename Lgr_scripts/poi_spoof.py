# %%
import sklearn as sk
import pandas as pd
from sklearn.linear_model import LogisticRegression

# %%
import math
def sigmoid(x):
  return 1 / (1 + math.exp(-x))

# %%

thresh = pd.read_csv("/mount_point/Madhuri/new/de_tresh.csv")

# %%
thresh.head()
# p_thresh = pd.read_csv("/home/arun/Downloads/Lgr/de_tresh.csv")

# # %%
# p_thresh.head()
# %%
#print(thresh['Type'].value_counts())

# %%
def analysis(messi,a,b,name):
    
    y = messi.iloc[:,a] 
    X = messi.iloc[:,b]

    print(X.head(3))
    
    print(" ++++++++++++++++ ")
    
    print(y.head(3))
    
    print(" ++++++++++++++++ ")
    
    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(X,y, random_state=0)
    
    LR = LogisticRegression(random_state=0, solver='lbfgs', multi_class='ovr').fit(X_train, y_train)
    #print(LR.predict(X_test))
    print("train accuracy",LR.score(X,y))
    print("test accuracy",LR.score(X_test,y_test))

    print(LR.intercept_)
    print(LR.coef_)
    print(LR.classes_)
    
    n = len(b)
    #print(n)
    #print("hi==",(LR.coef_[0][1]))
    equation = ""
    cols = messi.columns
    for i in range(n):
        #print("i==",i)
        temp = str(LR.coef_[0][i])
        #print(temp)
        temp = temp + "*"+str(name)+"['"+str(cols[b[i]])+"']"
        #print(temp)
        equation += temp
    
    equation = equation +"+"+ str(LR.intercept_[0])
    
    print(" ")
    print(" ")

    print("equation for sigmoid is == ",equation)      
        

# %%
analysis(thresh,[5],[1,2],"thresh") #old model

# # %%
analysis(thresh,[5],[3,4],"thresh") #new model

#%%

# # %%
test = pd.read_csv("/home/arun/Downloads/Lgr/qr_test.csv")
test['sigmoid_new'] = -3.685837811290938*thresh['New_PS']-6.8202421763911705*thresh['New_DS']+5.642453689126522
# %%
test['sigmoid_old'] = -0.7642268173775765*thresh['Prod_PS']-3.95505428486643*thresh['Prod_DS']+5.115969005784824

# # test['sigmoid_new'] = -4.9497489312389655*thresh['New_PS']-8.355942297020901*thresh['New_DS']+5.266077290806263
# # # # %%
# # test['sigmoid_old'] = -4.365100837680614*thresh['Prod_PS']-6.332003553869409*thresh['Prod_DS']+4.703378090728829
# # # # %%
test.head()

# %%
print("oldfake",len(test[(test['sigmoid_old'] < 0) & (test['Type'] == "Fake")]) , len(test[(test['sigmoid_old'] > 0) & (test['Type'] == "Fake")]))


# %%
print("newfake",len(test[(test['sigmoid_new'] < 0) & (test['Type'] == "Fake")]) , len(test[(test['sigmoid_new'] > 0) & (test['Type'] == "Fake")]))


# %%
print("oldreal",len(test[(test['sigmoid_old'] < 0) & (test['Type'] == "Real")]) , len(test[(test['sigmoid_old'] > 0) & (test['Type'] == "Real")]))


# %%
print("newreal",len(test[(test['sigmoid_new'] < 0) & (test['Type'] == "Real")]) , len(test[(test['sigmoid_new'] > 0) & (test['Type'] == "Real")]))



# %%
print(test['Type'].value_counts())

# %%
print(sigmoid(3))


